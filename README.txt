﻿Image Clipboard Tool

Simple tool to send images from a directory to the clipboard. 
Double click or Ctrl+c an image to send it to the clipboard.

Requires Microsoft .Net Framework 4.0