﻿namespace ImageClipboardTool
{
    partial class Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.acceptButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.librarySettingsBox = new System.Windows.Forms.GroupBox();
            this.manageLibraryButton = new System.Windows.Forms.Button();
            this.displaySettingsBox = new System.Windows.Forms.GroupBox();
            this.sizeLabel = new System.Windows.Forms.Label();
            this.sizeUpDown = new System.Windows.Forms.NumericUpDown();
            this.librarySettingsBox.SuspendLayout();
            this.displaySettingsBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sizeUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // acceptButton
            // 
            this.acceptButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.acceptButton.Location = new System.Drawing.Point(12, 138);
            this.acceptButton.Name = "acceptButton";
            this.acceptButton.Size = new System.Drawing.Size(75, 23);
            this.acceptButton.TabIndex = 0;
            this.acceptButton.Text = "Save";
            this.acceptButton.UseVisualStyleBackColor = true;
            this.acceptButton.Click += new System.EventHandler(this.AcceptButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(97, 138);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 1;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // librarySettingsBox
            // 
            this.librarySettingsBox.Controls.Add(this.manageLibraryButton);
            this.librarySettingsBox.Location = new System.Drawing.Point(12, 13);
            this.librarySettingsBox.Name = "librarySettingsBox";
            this.librarySettingsBox.Size = new System.Drawing.Size(160, 57);
            this.librarySettingsBox.TabIndex = 2;
            this.librarySettingsBox.TabStop = false;
            this.librarySettingsBox.Text = "Library Settings";
            // 
            // manageLibraryButton
            // 
            this.manageLibraryButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.manageLibraryButton.Location = new System.Drawing.Point(6, 19);
            this.manageLibraryButton.Name = "manageLibraryButton";
            this.manageLibraryButton.Size = new System.Drawing.Size(148, 23);
            this.manageLibraryButton.TabIndex = 1;
            this.manageLibraryButton.Text = "Manage Library";
            this.manageLibraryButton.UseVisualStyleBackColor = true;
            this.manageLibraryButton.Click += new System.EventHandler(this.ManageLibraryButton_Click);
            // 
            // displaySettingsBox
            // 
            this.displaySettingsBox.Controls.Add(this.sizeLabel);
            this.displaySettingsBox.Controls.Add(this.sizeUpDown);
            this.displaySettingsBox.Location = new System.Drawing.Point(12, 76);
            this.displaySettingsBox.Name = "displaySettingsBox";
            this.displaySettingsBox.Size = new System.Drawing.Size(160, 54);
            this.displaySettingsBox.TabIndex = 3;
            this.displaySettingsBox.TabStop = false;
            this.displaySettingsBox.Text = "Display Settings";
            // 
            // sizeLabel
            // 
            this.sizeLabel.AutoSize = true;
            this.sizeLabel.Location = new System.Drawing.Point(6, 22);
            this.sizeLabel.Name = "sizeLabel";
            this.sizeLabel.Size = new System.Drawing.Size(47, 13);
            this.sizeLabel.TabIndex = 4;
            this.sizeLabel.Text = "Tile Size";
            // 
            // sizeUpDown
            // 
            this.sizeUpDown.Location = new System.Drawing.Point(85, 20);
            this.sizeUpDown.Maximum = new decimal(new int[] {
            1024,
            0,
            0,
            0});
            this.sizeUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.sizeUpDown.Name = "sizeUpDown";
            this.sizeUpDown.Size = new System.Drawing.Size(68, 20);
            this.sizeUpDown.TabIndex = 3;
            this.sizeUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // Settings
            // 
            this.AcceptButton = this.acceptButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(184, 173);
            this.Controls.Add(this.displaySettingsBox);
            this.Controls.Add(this.librarySettingsBox);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.acceptButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Settings";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Settings";
            this.librarySettingsBox.ResumeLayout(false);
            this.displaySettingsBox.ResumeLayout(false);
            this.displaySettingsBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sizeUpDown)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button acceptButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.GroupBox librarySettingsBox;
        private System.Windows.Forms.Button manageLibraryButton;
        private System.Windows.Forms.GroupBox displaySettingsBox;
        private System.Windows.Forms.Label sizeLabel;
        private System.Windows.Forms.NumericUpDown sizeUpDown;
    }
}