﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.IO;
using System.Windows.Forms;
using System.Drawing.Drawing2D;


namespace ImageClipboardTool
{
    public static class ImageProcessor
    {
        private static readonly string[] VALID_EXTENSIONS = { ".png", ".jpg", ".jpeg", "*.jfif", ".bmp", ".tif", ".tiff", ".gif" };

        /// <summary>
        /// Change the dimensions of an image to a square without scaling the source image
        /// </summary>
        /// <param name="image">Image to resize</param>
        /// <returns>Resized image</returns>
        public static Image ResizeToSquare(Image image, int size) 
        {
            var originalSize = Math.Max(image.Width, image.Height);
            var resizeRatio = size / originalSize;
            
            Image newImage = new Bitmap(size, size, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

            using (var graphics = Graphics.FromImage(newImage)) {
                graphics.Clear(Color.Transparent);
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBilinear;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                int offsetX = 0;
                int offsetY = 0;
                int newWidth = size;
                int newHeight = size;

                // Set offset for the resized dimension
                if (image.Width > image.Height) {
                    float offset = (float)(image.Width - image.Height) / 2 * size / originalSize;
                    offsetY = (int)offset;

                    float ratio = (float)image.Height / image.Width;
                    newHeight = (int)(size * ratio);
                } else if (image.Width < image.Height) {
                    float offset = (float)(image.Height - image.Width) / 2 * size / originalSize;
                    offsetX = (int)offset;

                    float ratio = (float)image.Width / image.Height;
                    newWidth = (int)(size * ratio);
                }

                var imageDestination = new Rectangle(offsetX, offsetY, newWidth, newHeight);
                graphics.DrawImage(image, imageDestination, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel);
            }
            return newImage;
        }

        /// <summary>
        /// Get a list of all image files within a directory
        /// </summary>
        /// <param name="path">Directory to search</param>
        /// <returns>List of image files</returns>
        public static List<string> GetImageFilesFromDirectory(string path)
        {
            var imageFiles = new List<string>();
            if (path.Length != 0) {
                var allFiles = Directory.GetFiles(path);
                foreach (var file in allFiles) {
                    string extension = Path.GetExtension(file);
                    if (VALID_EXTENSIONS.Contains(extension.ToLower())) {
                        imageFiles.Add(file);
                    }
                }
            } else {
#if DEBUG
                Console.WriteLine("GetImageFilesFromDirectory: Missing path");
#endif
            }
            return imageFiles;
        }

        /// <summary>
        /// Build ImageList based on a list of files and image size
        /// </summary>
        /// <param name="files">List of file paths to load</param>
        /// <param name="size">Width and Height of each icon in pixels</param>
        /// <returns></returns>
        public static ImageList BuildImageList(List<string> files, int size)
        {
            var imageList = new ImageList();
            imageList.ImageSize = new Size(size, size);
            foreach (var file in files) {
                using (var image = Image.FromFile(file)) {
                    try {
                        imageList.Images.Add(ResizeToSquare(image, size));
#if DEBUG
                        Console.WriteLine("GetImageList: Added {0}", file);
#endif
                    } catch (Exception e) {
#if DEBUG
                        Console.WriteLine("Problem loading {0}", file);
                        Console.WriteLine(e);
#endif
                        continue;
                    }
                }
            }
            return imageList;
        }
    }
}
