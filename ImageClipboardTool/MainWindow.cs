﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using System.Linq;

namespace ImageClipboardTool
{
    public partial class ImageClipboardTool : Form
    {
        private static Image FOLDER_ICON = Properties.Resources.folder_icon;
        private string[] m_LibraryItems;
        private int m_Size = Properties.Settings.Default.Size;
        private List<string> m_FileList;

        public ImageClipboardTool()
        {
            InitializeComponent();

            // Load settings
            m_LibraryItems = Properties.Settings.Default.LibraryItems.Split(',');
            // Build list
            SwitchToLibraryView();
        }

        /// <summary>
        /// Handle ctrl-c keypress to copy to clipboard and esc to go back to library view
        /// </summary>
        private void ImageListView_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 3 && ModifierKeys == Keys.Control) {
                var indicies = imageListView.SelectedIndices;
                if (indicies.Count != 0) {
                    // Can only select one in listview
                    CopyToClipboard(indicies[0]);
                }
            } else if (e.KeyChar == 27) {
                SwitchToLibraryView();
            }
        }

        private void LibraryListView_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            var senderList = (ListView)sender;
            var clickedItem = senderList.HitTest(e.Location).Item;
            if (clickedItem != null) {
                //CopyToClipboard(clickedItem.Index);
                SwitchToImageView(m_LibraryItems[clickedItem.Index].Split(';')[1]);
            }
        }

        /// <summary>
        /// Handle double click to copy to clipboard
        /// </summary>
        private void ImageListView_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            var senderList = (ListView)sender;
            var clickedItem = senderList.HitTest(e.Location).Item;
            if (clickedItem != null) {
                CopyToClipboard(clickedItem.Index);
            }
        }

        /// <summary>
        /// Switch to library view when clicked
        /// </summary>
        private void HomeButton_Click(object sender, EventArgs e)
        {
            SwitchToLibraryView();
        }

        /// <summary>
        /// Open settings dialog and rebuild ImageListView if settings have been changed
        /// </summary>
        private void SettingsButton_Click(object sender, EventArgs e)
        {
            var settings = new Settings();
            bool rebuild = false;
            if (settings.ShowDialog() == DialogResult.OK) {
                // if library has changed, update program setting
                var items = Properties.Settings.Default.LibraryItems.Split(',');

                if (m_LibraryItems != items) {
                    m_LibraryItems = items;
                    rebuild = true;
#if DEBUG
                    Console.WriteLine("Updated libraries.");
#endif
                }

                // if size changes, update program setting
                if (Properties.Settings.Default.Size != m_Size) {
                    m_Size = Properties.Settings.Default.Size;
#if DEBUG
                    Console.WriteLine("Updated size.");
#endif
                    // fix setting if size < 1
                    if (m_Size < 1) {
                        m_Size = 1;
                        Properties.Settings.Default.Size = 1;
                        Properties.Settings.Default.Save();
#if DEBUG
                        Console.WriteLine("WARNING: Size setting is less than 1. Setting Size to 1.");
#endif
                    }
                    rebuild = true;
                }
                if (rebuild) {
                    SwitchToLibraryView();
                }
            }
        }

        /// <summary>
        /// Open About dialog
        /// </summary>
        private void AboutButton_Click(object sender, EventArgs e)
        {
            var about = new About();
            about.Show();
        }

        /// <summary>
        /// Copy image at index to clipboard
        /// </summary>
        /// <param name="index">Index of image</param>
        private void CopyToClipboard(int index)
        {
            if (m_FileList != null && m_FileList.Count != 0) {
                try {
                    var fileName = Path.GetFileNameWithoutExtension(m_FileList[index]);
#if DEBUG
                    Console.WriteLine("Added {0} with index {1} to clipboard", fileName, index);
#endif
                    outputLabel.Text = "Copied '" + fileName + "' to the clipboard.";
                    Clipboard.SetImage(Image.FromFile(m_FileList[index]));
                } catch (IndexOutOfRangeException e) {
#if DEBUG
                    Console.WriteLine("Index {0} out of bounds.\n{1}", index, e);
#endif
                }
            }
        }

        /// <summary>
        /// Build imageListView using folders from library
        /// </summary>
        private void BuildLibraryListView()
        {
            if (Properties.Settings.Default.LibraryItems.Length != 0) {
                imageListView.Items.Clear();
                if (imageListView.LargeImageList == null) {
                    imageListView.LargeImageList = new ImageList();
                    imageListView.LargeImageList.ImageSize = new Size(m_Size, m_Size);
                }

                // Set icon to folder
                imageListView.LargeImageList.Images.Clear();
                imageListView.LargeImageList.Images.Add(FOLDER_ICON);

                // Add all folders
                foreach (var folder in m_LibraryItems) {
                    imageListView.Items.Add(folder.Split(';')[0], 0);
                }
            }
        }

        /// <summary>
        /// Build imageListView using files from path
        /// </summary>
        private void BuildImageListView(string path)
        {
            imageListView.Items.Clear();
            if (imageListView.LargeImageList != null) {
                imageListView.LargeImageList.Dispose();
            }

            m_FileList = ImageProcessor.GetImageFilesFromDirectory(path);
            if (m_FileList.Count() != 0) {
                var images = ImageProcessor.BuildImageList(m_FileList, m_Size);

                imageListView.LargeImageList = images;

                // Add images to listview
                for (var i = 0; i < m_FileList.Count; i++) {
                    imageListView.Items.Add(Path.GetFileNameWithoutExtension(m_FileList[i]), i);
                }
            }
        }

        /// <summary>
        /// Switch form events to facilitate folder selection
        /// </summary>
        private void SwitchToLibraryView()
        {
#if DEBUG
            Console.WriteLine("Switching to folder view.");
#endif
            imageListView.MouseDoubleClick -= ImageListView_MouseDoubleClick;
            imageListView.KeyPress -= ImageListView_KeyPress;
            imageListView.MouseDoubleClick += LibraryListView_MouseDoubleClick;
            BuildLibraryListView();
        }

        /// <summary>
        /// Switch form events to facilitate image selection
        /// </summary>
        /// <param name="path">Library folder path</param>
        private void SwitchToImageView(string path)
        {
            if (path.Length != 0) {
#if DEBUG
                Console.WriteLine("Switching to image folder '{0}'.", path);
#endif
                imageListView.MouseDoubleClick -= LibraryListView_MouseDoubleClick;
                imageListView.MouseDoubleClick += ImageListView_MouseDoubleClick;
                imageListView.KeyPress += ImageListView_KeyPress;
                BuildImageListView(path);
            } else {
#if DEBUG
                Console.WriteLine("Path is empty");
#endif
            }
        }
    }
}
