﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ImageClipboardTool
{
    public static class LibraryInputBox
    {
        /// <summary>
        /// Show library input dialog to get library item
        /// </summary>
        /// <param name="title">Title of dialog</param>
        /// <param name="value">Ref of library item value</param>
        /// <param name="libraryName">Name of library</param>
        /// <param name="path">Path of library</param>
        /// <returns>DialogResult</returns>
        public static DialogResult ShowDialog(string title, ref string value, string libraryName = "", string path = "")
        {
            Form form = new Form();
            form.SuspendLayout();
            #region Windows Form Designer generated code

            /// <summary>
            /// Required method for Designer support - do not modify
            /// the contents of this method with the code editor.
            /// </summary>
            TextBox textBox1;
            Label label1;
            Button folderBrowserButton;
            Button cancelButton;
            Button okButton;
            textBox1 = new TextBox();
            label1 = new Label();
            folderBrowserButton = new Button();
            cancelButton = new Button();
            okButton = new Button();
            // 
            // textBox1
            // 
            textBox1.Location = new Point(12, 41);
            textBox1.Name = "textBox1";
            textBox1.Size = new Size(312, 20);
            textBox1.TabIndex = 0;
            textBox1.Text = libraryName;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(12, 25);
            label1.Name = "label1";
            label1.Size = new Size(72, 13);
            label1.TabIndex = 1;
            label1.Text = "Library Name:";
            // 
            // folderBrowserButton
            // 
            folderBrowserButton.Location = new Point(330, 39);
            folderBrowserButton.Name = "folderBrowserButton";
            folderBrowserButton.Size = new Size(23, 23);
            folderBrowserButton.TabIndex = 2;
            folderBrowserButton.Text = "📂 ";
            folderBrowserButton.UseVisualStyleBackColor = true;
            folderBrowserButton.Click += (o, e) =>
            {
                var dialog = new FolderBrowserDialog();
                dialog.SelectedPath = path.Replace(@"\\", @"\");
                if (dialog.ShowDialog() == DialogResult.OK && dialog.SelectedPath.Length != 0) {
                    path = dialog.SelectedPath.Replace(@"\", @"\\");
                }
            };
            // 
            // cancelButton
            // 
            cancelButton.Anchor = ((AnchorStyles)((AnchorStyles.Bottom | AnchorStyles.Right)));
            cancelButton.Location = new Point(278, 76);
            cancelButton.Name = "cancelButton";
            cancelButton.Size = new Size(75, 23);
            cancelButton.TabIndex = 3;
            cancelButton.Text = "Cancel";
            cancelButton.UseVisualStyleBackColor = true;
            // 
            // okButton
            // 
            okButton.Anchor = ((AnchorStyles)((AnchorStyles.Bottom | AnchorStyles.Right)));
            okButton.Location = new Point(197, 76);
            okButton.Name = "okButton";
            okButton.Size = new Size(75, 23);
            okButton.TabIndex = 4;
            okButton.Text = "OK";
            okButton.UseVisualStyleBackColor = true;
            okButton.Click += (o, e) =>
            {
                if (textBox1.Text.Length != 0 && path.Length != 0) {
                    form.DialogResult = DialogResult.OK;
                    form.Close();
                } else {
                    MessageBox.Show("Invalid entry.\nName: " + textBox1.Text + "\nFolder: " + path);
                }
            };
            // 
            // InputLibraryItem
            // 
            form.AutoScaleDimensions = new SizeF(6F, 13F);
            form.AutoScaleMode = AutoScaleMode.Font;
            form.ClientSize = new Size(362, 111);
            form.Controls.Add(okButton);
            form.Controls.Add(cancelButton);
            form.Controls.Add(folderBrowserButton);
            form.Controls.Add(label1);
            form.Controls.Add(textBox1);
            form.Name = "InputLibraryItem";
            form.Text = title;
            form.CancelButton = cancelButton;
            form.AcceptButton = okButton;
            form.MaximizeBox = false;
            form.MinimizeBox = false;
            form.FormBorderStyle = FormBorderStyle.FixedToolWindow;
            form.ShowIcon = false;
            form.ShowInTaskbar = false;
            form.StartPosition = FormStartPosition.CenterParent;
            form.ResumeLayout(false);
            form.PerformLayout();

            #endregion

            DialogResult dialogResult = form.ShowDialog();
            
            if (dialogResult == DialogResult.OK) {
                value = String.Join(";", textBox1.Text, path);
            }
            return dialogResult;
        }
    }
}
