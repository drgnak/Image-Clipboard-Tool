﻿using System;
using System.Windows.Forms;
using System.IO;
namespace ImageClipboardTool
{
    public partial class About : Form
    {
        public About()
        {
            InitializeComponent();

            // Get version
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            var info = System.Diagnostics.FileVersionInfo.GetVersionInfo(assembly.Location);
            versionLabel.Text = "Version: " + info.FileVersion;

            // Get build time
            DateTime buildDate = new FileInfo(System.Reflection.Assembly.GetExecutingAssembly().Location).LastWriteTime;
            buildDateLabel.Text = "Build date: " + buildDate;
        }

        private void LicenseButton_Click(object sender, EventArgs e)
        {
            var license = new License();
            license.Show();
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://gitlab.com/u/drgnak");
        }


    }
}
