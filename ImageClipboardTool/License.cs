﻿using System;
using System.Windows.Forms;

namespace ImageClipboardTool
{
    public partial class License : Form
    {
        public License()
        {
            InitializeComponent();
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
