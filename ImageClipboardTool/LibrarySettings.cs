﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ImageClipboardTool
{
    public partial class LibrarySettings : Form
    {
        private List<string> m_LibraryItems;
        public LibrarySettings(string libraryItems)
        {
            InitializeComponent();

            if (libraryItems.Length != 0) {
                m_LibraryItems = libraryItems.Split(',').ToList<string>();
            } else {
                m_LibraryItems = new List<string>();
            }
            libraryListBox.DataSource = m_LibraryItems;
        }

        /// <summary>
        /// ShowDialog with library item ref to pass library settings
        /// </summary>
        /// <param name="value">Library item string reference</param>
        /// <returns>DialogResult</returns>
        public DialogResult ShowDialog(ref string value)
        {
            var dialogResult = ShowDialog();
            value = String.Join(",", m_LibraryItems);
            return dialogResult;
        }

        /// <summary>
        /// Add item to list
        /// </summary>
        private void AddButton_Click(object sender, EventArgs e)
        {
            string value = "";
            if (LibraryInputBox.ShowDialog("Add Library", ref value) == DialogResult.OK) {
                m_LibraryItems.Add(value);
                UpdateListBox();
            }
        }

        /// <summary>
        /// Edit item in list
        /// </summary>
        private void EditButton_Click(object sender, EventArgs e)
        {
            string value = "";
            var index = libraryListBox.SelectedIndex;
            var item = m_LibraryItems[index].Split(';');
            if (LibraryInputBox.ShowDialog("Edit Library", ref value, item[0], item[1]) == DialogResult.OK) {
                m_LibraryItems[index] = value;
                UpdateListBox();
            }
        }

        /// <summary>
        /// Delete item in list
        /// </summary>
        private void DeleteButton_Click(object sender, EventArgs e)
        {
            var index = libraryListBox.SelectedIndex;
            if (index > -1) {
                m_LibraryItems.RemoveAt(index);
            }
            UpdateListBox();
        }

        /// <summary>
        /// Send DialogResult.OK and close form
        /// </summary>
        private void SaveButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            Close();
        }

        /// <summary>
        /// Update ListBox with new contents
        /// </summary>
        private void UpdateListBox()
        {
            libraryListBox.DataSource = null;
            libraryListBox.DataSource = m_LibraryItems;
        }
    }
}
