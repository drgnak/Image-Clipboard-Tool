﻿using System;
using System.Windows.Forms;

namespace ImageClipboardTool
{
    public partial class Settings : Form
    {
        private string m_LibraryItems;

        public Settings()
        {
            InitializeComponent();
            // Load settings
            sizeUpDown.Value = Properties.Settings.Default.Size;
            m_LibraryItems = Properties.Settings.Default.LibraryItems;
        }

        /// <summary>
        /// Button event handler to get a new image folder path from user
        /// </summary>
        private void ManageLibraryButton_Click(object sender, EventArgs e)
        {
            var dialog = new LibrarySettings(Properties.Settings.Default.LibraryItems);
            string value = "";
            if (dialog.ShowDialog(ref value) == DialogResult.OK) {
                m_LibraryItems = value;
            }
            this.Activate();
        }

        /// <summary>
        /// Button event handler to save all settings
        /// </summary>
        private void AcceptButton_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.Size = (int)sizeUpDown.Value;
            Properties.Settings.Default.LibraryItems = m_LibraryItems;
            Properties.Settings.Default.Save();
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
